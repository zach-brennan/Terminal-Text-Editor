run: editor.c
	ls ./*.[c] | xargs clang-format -i -style=file
	gcc editor.c -o tte -Wall -Wextra -pedantic -std=c99
	chmod +x ./tte
	cp ./tte ~/bin

build: editor.c
	gcc editor.c -o tte -Wall -Wextra -pedantic -std=c99
