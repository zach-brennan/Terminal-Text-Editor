# Terminal Text Editor
A simple terminal-based text editor written in C.

## Prerequisites

The required software packages to build this project are:

```
- make
- gcc
```
## Build
The default target for the included `Makefile` will compile the editor into an executable named `tte`, and then copy it to the `~/bin` directory, so that it can be run from any location. If you would prefer to build it without copying it as such, the `make build` command will leave the executable in the current directory.

## Usage
```
tte [filename]
  -filename: file to be opened. If the file does not exist,
   it is then created.
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


## Notes

A large majority of the design for this editor came from following the [Build Your Own Text Editor](https://viewsourcecode.org/snaptoken/kilo/).
The idea behind doing this was to get a functional text editor that I understood at the very base level, and could 
therefore edit and change to suit the way that I want it to work. 
